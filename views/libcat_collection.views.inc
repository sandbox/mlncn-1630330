<?php

/**
 * @file
 * Views support.
 */

/**
 * Implements hook_views_data().
 */
function libcat_collection_views_data() {
  // Define the base group of this table.
  $data['libcat_collection']['table']['group']  = t('Collection');

  // Advertise this table as a possible base table
  $data['libcat_collection']['table']['base'] = array(
    'field' => 'collection_id',
    'title' => t('Collection'),
    'weight' => -10,
  );
  $data['libcat_collection']['table']['entity type'] = 'libcat_collection';
  $data['libcat_collection']['collection_id'] = array(
    'title' => t('Collection ID'),
    'help' => t('The libcat_collection ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['libcat_collection']['collection_type'] = array(
    'title' => t('Collection type'),
    'help' => t('The libcat_collection type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'libcat_collection_type_names',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['libcat_collection']['collection_vid'] = array(
    'title' => t('Vid'),
    'help' => t('The libcat_collection revision ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['libcat_collection']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('The libcat_collection uid.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'label' => 'libcat_collection owner',
      'title' => t('Collection owner'),
      'base' => 'users',
      'base field' => 'uid',
    ),
  );

  $data['libcat_collection']['updated'] = array(
    'title' => t('Updated Date'),
    'help' => t('The date when the libcat_collection was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['libcat_collection']['name'] = array(
    'title' => t('Name'),
    'help' => t('The name of this libcat_collection.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_in_operator',
      'options callback' => 'libcat_collection_return_names',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
