<?php
/**
 * @file
 * Administration pages for managing Library Catalog collections.
 */

/**
 * Menu callback: manage library catalog collection requests.
 */
function libcat_collection_admin_collections_form($form, $form_state) {
  $form['filter'] = libcat_collection_filter_form();
  $form['#submit'][] = 'libcat_collection_filter_form_submit';
  $form['admin'] = libcat_collection_manage_form();

  return $form;
}

/**
 * Form builder: Builds the collection administration overview.
 *
 * Modeled on node_admin_nodes().  Unlike the node administration or library
 * catalog order administration forms, we do not have a need for bulk operations
 * on collections.  Which means this is not technically a form that has to be
 * submitted, but tablesort works just as well here so don't stress it.
 */
function libcat_collection_manage_form() {

  // Build the sortable table header.
  $header = array(
    'name' => array('data' => t('Name'), 'field' => 'c.name'),
    'type' => array('data' => t('Type'), 'field' => 'c.collection_type'),
    'updated' => array('data' => t('Updated date'), 'field' => 'c.updated', 'sort' => 'desc')
  );
  $header['operations'] = array('data' => t('Operations'));

  $query = db_select('libcat_collection', 'c')->extend('PagerDefault')->extend('TableSort');
  libcat_collection_build_filter_query($query);

  $collections = $query
    ->fields('c', array('collection_id', 'name', 'collection_type', 'updated'))
    ->limit(50)
    ->orderByHeader($header)
    ->execute()
    ->fetchAll();

  // Prepare the list of collections.
  $destination = drupal_get_destination();
  $rows = array();
  foreach ($collections as $collection) {
    $rows[$collection->collection_id] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $collection->name,
          '#href' => 'libcat/collection/' . $collection->collection_id,
        ),
      ),
      'type' => $collection->collection_type,
      'updated' => format_date($collection->updated, 'short'),
    );
    // Build a list of all the accessible operations for the current node.
    $operations = array();
    $available_operations = array(
      'edit' => array(
        'label' => t('edit'),
        'tooltip' => t('Edit collection information.'),
      ),
    );
    foreach ($available_operations as $key => $op) {
      $operations[$key] = array(
        'title' => $op['label'],
        'href' => 'libcat/collection/' . $collection->collection_id . '/' . $key,
        'query' => $destination,
        'attributes' => array('title' => $op['tooltip']),
      );
    }
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $rows[$collection->collection_id]['operations'] = array(
        'data' => array(
          '#theme' => 'links__libcat_collection_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $rows[$collection->collection_id]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  $form['collections'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No collections.'),
  );

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Return form for collection administration filters.
 */
function libcat_collection_filter_form() {
  $session = isset($_SESSION['libcat_collection_filter']) ? $_SESSION['libcat_collection_filter'] : array();
  $filters = libcat_collection_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__libcat_collection',
  );
  $multiple_filters = count($session) > 1 ? TRUE : FALSE;
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $value = $filters[$type]['options'][$value];
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    // Prevent trying to filter twice on option that can have only one setting.
    if (in_array($type, array('type'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => (($multiple_filters && $i) ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js');

  return $form;
}

/**
 * Process result from libcat collections administration filter form.
 */
function libcat_collection_filter_form_submit($form, &$form_state) {
  $filters = libcat_collection_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION['libcat_collection_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['libcat_collection_filter']);
      break;
    case t('Reset'):
      $_SESSION['libcat_collection_filter'] = array();
      break;
  }
}

/**
 * List collection administration filters that can be applied.
 */
function libcat_collection_filters() {
  // Filter by type.
  $filters['type'] = array(
    'title' => t('Collection type'),
    'options' => array(
      '[any]' => t('any'),
    ) + libcat_collection_type_names(),
  );

  return $filters;
}

/**
 * Apply filters for library catalog collection management based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */
function libcat_collection_build_filter_query(SelectQueryInterface $query) {
  // Build query
  $filter_data = isset($_SESSION['libcat_collection_filter']) ? $_SESSION['libcat_collection_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'type':
        $query->condition('c.collection_type', $value);
        break;
    }
  }
}
