<?php

/**
 * @file
 * Default theme implementation to display a Library Catalog Collection.
 *
 * Available variables:
 * - $name: the (sanitized) name of the collection.
 * - $content: An array of collection items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $username: Themed username of collection owner from theme_username().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - libcat-collection: The current template type, i.e., "theming hook".
 *   - libcat-collection-[type]: The current collection type. For example, if
 *     the collection is institutional, "libcat-collection-institutional".
 *   - node-teaser: Nodes in teaser form.
 *   - collection-unpublished: Unpublished, visible only to administrators.
 *
 * Other variables:
 * - $collection: Full collection object. Contains data that may not be safe.
 * - $type: Collection type, i.e. personal, institutional.
 * - $uid: User ID of the node author.
 * - $updated: Time the collection was most recently saved, as a Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $status: Flag for published status.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the collection a
 * corresponding variable is defined, e.g. $collection->description becomes
 * $description.  When needing to access a field's raw values,
 * developers/themers are strongly encouraged to use these variables. Otherwise
 * they will have to explicitly specify the desired field language, e.g.
 * $collection->description['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_libcat_collection()
 * @see template_process()
 */
?>
<div id="libcat-collection-<?php print $collection->collection_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <h2><?php print $name; ?></h2>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide links now so that we can render them later.
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php print render($content['links']); ?>

</div>