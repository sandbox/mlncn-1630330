<?php

/**
 * @file
 * Page callbacks for reports section.
 */

/**
 * Lists existing collection types.
 */
function libcat_collection_overview_types() {
  $items = array();
  foreach (libcat_collection_types() as $type => $info) {
    $items[] = $info->name . ': ' . l(t('Add %type collection', array('%type' => $type)), 'libcat/collection/add/' . $type, array('html' => TRUE));
  }
  return theme('item_list', array('items' => $items));
}
