<?php

/**
 * @file
 * Page callbacks for library catalog collection pages associated with users.
 */

/**
 * Menu callback; presents the collection on user pages.
 */
function libcat_collection_user_page($account) {
  $build = array();
  if ($collection = libcat_collection_load_by_type_uid('personal', $account->uid)) {
    $build['edit'] = array(
      '#markup' => '<p>' . l(t('Edit collection settings'), 'libcat/collection/' . $collection->collection_id . '/edit') . '</p>',
    );
    $build['personal_collection'] = libcat_collection_view($collection);
  }
  return $build;
}
