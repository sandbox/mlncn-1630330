<?php

/**
 * @file
 *   Class(es) and methods for extending the entity controller for Components.
 */

/**
 * Define the entity controller for Components.
 *
 * This extends the DrupalDefaultEntityController class, adding required
 * special handling for library collection objects.
 */
class LibcatCollectionController extends DrupalDefaultEntityController {

  /**
   * Create a new instance of the Component controller.
   */
  public function create ($type = '') {
    return (object) array(
      'collection_id' => '',
      'collection_type' => '',
      'uid' => '',
      'collection_name' => '',
    );
  }
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    // Ensure that uid is taken from the {libcat_collection} table,
    $query = parent::buildQuery($ids, $conditions, $revision_id);
    $fields =& $query->getFields();
    $fields['uid']['table'] = 'base';
    $query->addField('revision', 'uid', 'revision_uid');
    return $query;
  }
}