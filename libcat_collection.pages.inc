<?php
/**
 * @file
 * Page callbacks for adding, editing, deleting, and revisions management for collections.
 */

/**
 * Menu callback; presents the collection editing form.
 */
function libcat_collection_edit_page($collection) {
  $type_label = $collection->collection_type;
  drupal_set_title(t('<em>Edit @type</em> @name', array('@type' => $type_label, '@name' => $collection->name)), PASS_THROUGH);
  return drupal_get_form($collection->collection_type . '_libcat_collection_form', $collection);
}

/**
 * Menu callback; presents the list of collections that can be added.
 */
function libcat_collection_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  // Bypass the node/add listing if only one collection type is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }
  return theme('node_add_list', array('content' => $content));
}

/**
 * Returns HTML for list of available collection types for creating collections.
 *
 * @param $variables
 *   An associative array containing:
 *   - content: An array of collection types.
 *
 * @ingroup themeable
 */
function theme_libcat_collection_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="node-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('You have not created any content types yet. Go to the <a href="@create-content">content type creation page</a> to add a new content type.', array('@create-content' => url('admin/structure/types/add'))) . '</p>';
  }
  return $output;
}


/**
 * Returns a collection creation form.
 */
function libcat_collection_add($type) {
  global $user;

  $types = libcat_collection_types();
  $collection = (object) array('uid' => $user->uid, 'username' => (isset($user->name) ? $user->name : ''), 'collection_type' => $type, 'language' => LANGUAGE_NONE);
  drupal_set_title(t('Create @name collection', array('@name' => $types[$type]->name)), PASS_THROUGH);
  $output = drupal_get_form($type . '_libcat_collection_form', $collection);

  return $output;
}

function libcat_collection_form_validate($form, &$form_state) {
  // $form_state['collection'] contains the actual entity being edited, but we
  // must not update it with form values that have not yet been validated, so we
  // create a pseudo-entity to use during validation.
  $collection = (object) $form_state['values'];
    // Validate the "authored by" field.
  if (!empty($collection->username) && !($account = user_load_by_name($collection->username))) {
    // The use of empty() is mandatory in the context of usernames
    // as the empty string denotes the anonymous user. In case we
    // are dealing with an anonymous user we set the user ID to 0.
    form_set_error('name', t('The username %name does not exist.', array('%name' => $node->name)));
  }
  else {
    $collection->uid = empty($collection->username) ? 0 : $account->uid;
    $form_state['values']['uid'] = $collection->uid;
  }

  // @TODO replace: node_validate($node, $form, $form_state);
  entity_form_field_validate('libcat_collection', $form, $form_state);
}

/**
 * Generate the collection add/edit form array.
 */
function libcat_collection_form($form, &$form_state, $collection) {
  global $user;

  // During initial form build, add the collection entity to the form state for use
  // during form building and processing. During a rebuild, use what is in the
  // form state.
  if (!isset($form_state['libcat_collection'])) {
    if (!isset($collection->name)) {
      $collection->name = libcat_collection_default_collection_name($collection->collection_type, $user->name);
    }
    libcat_collection_object_prepare($collection);
    $form_state['libcat_collection'] = $collection;
  }
  else {
    $collection = $form_state['libcat_collection'];
  }

  // Some special stuff when previewing a collection.
  if (isset($form_state['libcat_collection_preview'])) {
    $form['#prefix'] = $form_state['libcat_collection_preview'];
    $collection->in_preview = TRUE;
  }
  else {
    unset($collection->in_preview);
  }

  $form['#attributes']['class'][] = 'libcat-collection-form';
  if (!empty($collection->collection_type)) {
    $form['#attributes']['class'][] = 'libcat-collection-' . $collection->collection_type . '-form';
  }

  // Basic collection information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('collection_id', 'vid', 'uid', 'created', 'collection_type') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($collection->$key) ? $collection->$key : NULL,
    );
  }

  // Changed must be sent to the client, for later overwrite error checking.
  $form['updated'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($collection->updated) ? $collection->updated : NULL,
  );

  // All collections have a name.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection name'),
    '#required' => TRUE,
    '#default_value' => $collection->name,
    '#maxlength' => 255,
    '#weight' => -5,
  );

  // Collection (personal) owner information for administrators.
  $form['owner'] = array(
    '#type' => 'fieldset',
    '#access' => user_access(LIBCAT_ADMIN_ADMINISTER_CATALOG),
    '#title' => t('Owner information'),
    '#description' => t('Owner information is primary for personal collections, and a fallback (after managers) for institutional collections.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 90,
  );
  $form['owner']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Owned by'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => !empty($collection->uid) ? user_load($collection->uid)->name : '',
    '#weight' => -1,
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
  );

  // Add the buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('libcat_collection_form_submit'),
  );

  // Remove Delete button until we figure out best way to manage this - see Ticket #330
  // if (!empty($collection->collection_id) && libcat_collection_access('delete', $collection)) {
  //   $form['actions']['delete'] = array(
  //     '#type' => 'submit',
  //     '#value' => t('Delete'),
  //     '#weight' => 15,
  //     '#submit' => array('libcat_collection_form_delete_submit'),
  //   );
  // }

  field_attach_form('libcat_collection', $collection, $form, $form_state);
  return $form;
}

/**
 * Button submit function: handle the 'Delete' button on the node form.
 */
function libcat_collection_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $collection = $form['#collection'];
  $form_state['redirect'] = array('libcat/collection/' . $collection->collection_id . '/delete', array('query' => $destination));
}

function libcat_collection_form_submit($form, &$form_state) {
  $collection = libcat_collection_form_submit_build_collection($form, $form_state);
  // the above seems unnecessary.  Let's just get the collection.
  $collection_fs = $form_state['libcat_collection'];
  $insert = empty($collection->collection_id);
  libcat_collection_save($collection);
  $collection_link = l(t('view'), 'libcat/collection/' . $collection->collection_id);
  $watchdog_args = array('@type' => $collection->collection_type, '%name' => $collection->name);
  $t_args = array('@type' => libcat_collection_get_type_name($collection->collection_type), '%name' => $collection->name);
  if ($insert) {
    watchdog('libcat_collection', '@type: added %name.', $watchdog_args, WATCHDOG_NOTICE, $collection_link);
    drupal_set_message(t('@type collection %name has been created.', $t_args));
  }
  else {
    watchdog('libcat_collection', '@type: updated %name.', $watchdog_args, WATCHDOG_NOTICE, $collection_link);
    drupal_set_message(t('@type collection %name has been updated.', $t_args));
  }
  if ($collection->collection_id) {
    $form_state['values']['collection_id'] = $collection->collection_id;
    $form_state['collection_id'] = $collection->collection_id;
    $form_state['redirect'] = 'libcat/collection/' . $collection->collection_id;
  }
  else {
    // In the unlikely case something went wrong on save, the collection will be
    // rebuilt and node form redisplayed the same way as in preview. @TODO oops?
    drupal_set_message(t('The collection could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  // Clear the page and block caches.
  cache_clear_all();
}

/**
 * @TODO edit
 * Updates the form state's node entity by processing this submission's values.
 *
 * This is the default builder function for the node form. It is called
 * during the "Save" submit handler to retrieve the entity to
 * save. This function can also be called by a "Next" button of a
 * wizard to update the form state's entity with the current step's values
 * before proceeding to the next step.
 *
 * @see node_form()
 */
function libcat_collection_form_submit_build_collection($form, &$form_state) {
  $collection = $form_state['libcat_collection'];
  entity_form_submit_build_entity('libcat_collection', $collection, $form, $form_state);
  return $collection;
}

/**
 * Menu callback -- ask for confirmation of node deletion
 */
function libcat_collection_delete_confirm($form, &$form_state, $collection) {
  $form['#collection'] = $node;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['collection_id'] = array('#type' => 'value', '#value' => $collection->collection_id);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $collection->title)),
    'libcat/collection/' . $collection->collection_id,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete collection.
 */
function libcat_collection_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $collection = libcat_collection_load($form_state['values']['collection_id']);
    libcat_collection_delete($form_state['values']['collection_id']);
    watchdog('content', '@type: deleted %title.', array('@type' => $collection->type, '%name' => $collection->name));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $collection->collection_type, '%name' => $collection->name)));
  }

  $form_state['redirect'] = 'admin/libcat/collection';
}

/**
 * Generate an overview table of older revisions of a collection.
 * @todo
 */
function libcat_collection_revision_overview($node) {
  drupal_set_title(t('Revisions for %title', array('%title' => $node->title)), PASS_THROUGH);

  $header = array(t('Revision'), array('data' => t('Operations'), 'colspan' => 2));

  $revisions = node_revision_list($node);

  $rows = array();
  $revert_permission = FALSE;
  if ((user_access('revert revisions') || user_access('administer nodes')) && node_access('update', $node)) {
    $revert_permission = TRUE;
  }
  $delete_permission = FALSE;
  if ((user_access('delete revisions') || user_access('administer nodes')) && node_access('delete', $node)) {
    $delete_permission = TRUE;
  }
  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();

    if ($revision->current_vid > 0) {
      $row[] = array('data' => t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'short'), "node/$node->nid"), '!username' => theme('username', array('account' => $revision))))
                               . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : ''),
                     'class' => array('revision-current'));
      $operations[] = array('data' => drupal_placeholder(t('current revision')), 'class' => array('revision-current'), 'colspan' => 2);
    }
    else {
      $row[] = t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'short'), "node/$node->nid/revisions/$revision->vid/view"), '!username' => theme('username', array('account' => $revision))))
               . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : '');
      if ($revert_permission) {
        $operations[] = l(t('revert'), "node/$node->nid/revisions/$revision->vid/revert");
      }
      if ($delete_permission) {
        $operations[] = l(t('delete'), "node/$node->nid/revisions/$revision->vid/delete");
      }
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['node_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}

/**
 * Ask for confirmation of the reversion to prevent against CSRF attacks.
 * @todo
 */
function libcat_collection_revision_revert_confirm($form, $form_state, $node_revision) {
  $form['#node_revision'] = $node_revision;
  return confirm_form($form, t('Are you sure you want to revert to the revision from %revision-date?', array('%revision-date' => format_date($node_revision->revision_timestamp))), 'node/' . $node_revision->nid . '/revisions', '', t('Revert'), t('Cancel'));
}

/**
 * @todo
 */
function libcat_collection_revision_revert_confirm_submit($form, &$form_state) {
  $node_revision = $form['#node_revision'];
  $node_revision->revision = 1;
  $node_revision->log = t('Copy of the revision from %date.', array('%date' => format_date($node_revision->revision_timestamp)));

  node_save($node_revision);

  watchdog('content', '@type: reverted %title revision %revision.', array('@type' => $node_revision->type, '%title' => $node_revision->title, '%revision' => $node_revision->vid));
  drupal_set_message(t('@type %title has been reverted back to the revision from %revision-date.', array('@type' => node_type_get_name($node_revision), '%title' => $node_revision->title, '%revision-date' => format_date($node_revision->revision_timestamp))));
  $form_state['redirect'] = 'node/' . $node_revision->nid . '/revisions';
}

/**
 * @todo
 */
function libcat_collection_revision_delete_confirm($form, $form_state, $node_revision) {
  $form['#node_revision'] = $node_revision;
  return confirm_form($form, t('Are you sure you want to delete the revision from %revision-date?', array('%revision-date' => format_date($node_revision->revision_timestamp))), 'node/' . $node_revision->nid . '/revisions', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * @todo
 */
function libcat_collection_revision_delete_confirm_submit($form, &$form_state) {
  $node_revision = $form['#node_revision'];
  node_revision_delete($node_revision->vid);

  watchdog('content', '@type: deleted %title revision %revision.', array('@type' => $node_revision->type, '%title' => $node_revision->title, '%revision' => $node_revision->vid));
  drupal_set_message(t('Revision from %revision-date of @type %title has been deleted.', array('%revision-date' => format_date($node_revision->revision_timestamp), '@type' => node_type_get_name($node_revision), '%title' => $node_revision->title)));
  $form_state['redirect'] = 'node/' . $node_revision->nid;
  if (db_query('SELECT COUNT(vid) FROM {node_revision} WHERE nid = :nid', array(':nid' => $node_revision->nid))->fetchField() > 1) {
    $form_state['redirect'] .= '/revisions';
  }
}
