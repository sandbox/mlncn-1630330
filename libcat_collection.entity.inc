<?php

/**
 * @file
 * Entity boilerplate code.
 *
 * Except for general use hooks (in libcat_collection.module) and controller
 * class (in libcat_collection.controller.inc).
 */

/**
 * Implements hook_entity_info().
 */
function libcat_collection_entity_info() {
  $entities = array();
  $entities['libcat_collection'] = array(
    'label' => t('Collection'),
    'controller class' => 'LibcatCollectionController',
    'base table' => 'libcat_collection',
    'revision table' => 'libcat_collection_revision',
    'uri callback' => 'libcat_collection_uri',
    'label callback' => 'libcat_collection_label',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'collection_id',
      'revision' => 'collection_vid',
      'bundle' => 'collection_type',
      'label' => 'name',
    ),
    'bundle keys' => array(
      'bundle' => 'collection_type',
    ),
    'static cache' => TRUE,
    'bundles' => array(), // Defined dynamically below.
    'view modes' => array(
      'full' => array(
        'label' => t('Full page'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Summary teaser'),
        'custom settings' => TRUE,
      ),
    ),
  );

  foreach (libcat_collection_types() as $type => $info) {
    $entities['libcat_collection']['bundles'][$type] = array(
      'label' => $info->name,
      // No admin interface, which would be defined with an 'admin' array.
    );
  }

  return $entities;
}

/**
 * Callback function to provide the path to a given collection.
 */
function libcat_collection_uri($collection) {
  return array(
    'path' => 'libcat/collection/' . $collection->collection_id,
  );
}

/**
 * Callback function for the label (title) of a collection.
 *
 * Currently assumed never user-facing; will update as needed if not so.
 */
function libcat_collection_label($collection) {
  return $collection->name;
  // '(' . $collection->collection_type . ' for user number' . $collection->uid . ')';
}

/**
 * Get all types for Collection entities.
 *
 * This is used in libcat_collection_entity_info() to define the bundles.  We do not have
 * to be as complicated as the node_entity_info() version of this, which relies
 * on _node_types_build(), because we only allow programmatic, no UI direct to
 * database, creation of collection types.
 *
 * Note that we do not have an equivalent to node_type_save() for collections.
 * Instead of saving collection types to the database, they are defined entirely
 * in code.  Note this also means that field_attach_create_bundle() has no time
 * when it is called, which does not matter for SQL field storage but could be
 * required if we use another field storage back-end.
 */
function libcat_collection_types() {
  $types = &drupal_static(__FUNCTION__);
  if (!isset($types)) {
    $types = module_invoke_all('libcat_collection_info');
  }
  return $types;
}

/**
 * Loads a collection type, if it exists.
 *
 * @param string $type
 *   The machine name of the collection type to load.  If it is a URL-formatted
 *   version of the type name, dashes will be replaced with underscores.
 * @return array
 *   Returns an array of a collection type definition (the result of an
 *   implementation of hook_libcat_collection_info(), or FALSE if none exists.
 */
function libcat_collection_type_load($type) {
  $type = str_replace('-', '_', $type);
  $types = libcat_collection_types();
  return isset($types[$type]) ? $types[$type] : FALSE;
}

/**
 * Load a collection by type and user ID, or create collection if valid.
 */
function libcat_collection_load_or_create($collection_type, $uid = NULL) {
  $uid = $uid ? $uid : $GLOBALS['user']->uid;
  $collection = libcat_collection_load_by_type_uid($collection_type, $uid);
  if (!$collection) {
    $types = libcat_collection_types();
    if (!isset($types[$collection_type])) {
      // If this collection does not have a corresponding entity
      // collection type, do not try to create one and return FALSE.
      return FALSE;
    }
    $collection = libcat_collection_create($collection_type, user_load($uid));
  }
  return $collection;
}

/**
 * Create an empty collection given a collection type and a user.
 */
function libcat_collection_create($collection_type, $account = NULL, $name = '') {
  $account = ($account) ? $account : $GLOBALS['user'];
  $name = ($name) ? $name : libcat_collection_default_collection_name($collection_type, $account->name);
  $collection = (object) array(
    'collection_type' => $collection_type,
    'uid' => $account->uid,
    'name' => $name,
  );
  libcat_collection_save($collection, $account);
  return $collection;
}

/**
 * Helper function to return a collection name.
 *
 * @param string $collection_type
 * @param string $username
 *
 * @return string;
 */
function libcat_collection_default_collection_name($collection_type, $username) {
  $default_name = $username . "'s " . $collection_type . " collection";
  $context = array('collection_type' => $collection_type, 'username' => $username);
  drupal_alter('libcat_collection_default_collection_name', $default_name, $context);
  return $default_name;
}

/**
 * Given the machine name for a collection, get the human-friendly name.
 */
function libcat_collection_get_type_name($type) {
  $types = libcat_collection_types();
  return $types[$type]->name;
}

/**
 * Return available collection types in a machine name => display name array.
 *
 * Suitable for providing to select forms.
 */
function libcat_collection_type_names() {
  $names = array();
  $types = libcat_collection_types();
  foreach ($types as $type => $type_info) {
    $names[$type] = $type_info->name;
  }
  return $names;
}

/**
 * Return available collection names in a name => name  array
 * 
 * Suitable for providing to select forms
 */
function libcat_collection_return_names() {
  $names = array();
  $query = db_select('libcat_collection', 'l')
          ->fields('l', array('name'));
  $results = $query->execute();
  foreach ($results as $result) {
    $names[$result->name] = $result->name;
  }
  return $names;
}

/**
 * Return available collection names in a collection_id => name  array
 * 
 * Suitable for providing to select forms
 */
function libcat_collection_return_collection_id() {
  $names = array('' => 'Collections');
  $query = db_select('libcat_collection', 'l')
          ->fields('l', array('collection_id', 'name'));
  $results = $query->execute();
  foreach ($results as $result) {
    $names[$result->collection_id] = $result->name;
  }
  return $names;
}

/**
 * Return collection Owners in a uid => username array
 * 
 * Suitable for providing to select forms
 */
function libcat_collection_return_collection_owner() {
  $owners = array();
  $query = db_select('libcat_collection', 'l')
          ->fields('l', array('uid'));
  $query->join('users', 'u', 'u.uid=l.uid');
  $query->addField('u', 'name');
  $results = $query->execute();
  foreach($results as $result) {
    $owners[$result->uid] = $result->name;
  }
  return $owners;
}

/**
 * Save (update or insert) a business collection.
 *
 * @param object $collection
 *   A business collection object.
 * @param object $account
 *   Optional.  A user object of the user saving the collection.
 */
function libcat_collection_save($collection, $account = NULL) {
  if (!$account) {
    $account = $GLOBALS['user'];
  }

  // Always save a new revision.  If this becomes an option, change this line.
  $collection->revision = TRUE;

  $transaction = db_transaction();

  try {
    // Load the stored entity, if any.
    if (!empty($collection->collection_id) && !isset($collection->original)) {
      $collection->original = entity_load_unchanged('libcat_collection', $collection->collection_id);
    }
    field_attach_presave('libcat_collection', $collection);

    if (!isset($collection->is_new)) {
      $collection->is_new = empty($collection->collection_id);
    }

    if (!isset($collection->uid)) {
      $collection->uid = $account->uid;
    }

    // Set the timestamp fields.
    if (empty($collection->updated)) {
      $collection->updated = REQUEST_TIME;
    }
    // The changed timestamp is always updated for bookkeeping purposes,
    // for example: revisions, searching, etc.
    $collection->saved = REQUEST_TIME;

    $update_collection = TRUE;

    // Let modules modify the collection before it is saved to the database.
    module_invoke_all('libcat_collection_presave', $collection);
    module_invoke_all('entity_presave', $collection, 'libcat_collection');

    // When saving a new collection revision, unset any existing
    // $collection->collection_vid so as to ensure that a new revision will
    // be created, then store the old revision ID in a separate property for
    // collection or entity hook implementations.
    if (!$collection->is_new && !empty($collection->revision) && $collection->collection_vid) {
      $collection->old_vid = $collection->collection_vid;
      unset($collection->collection_vid);
    }

    // Save the collection and collection revision.
    if ($collection->is_new) {
      // For new collections, save new records for both the collection itself and
      // the collection revision.
      drupal_write_record('libcat_collection', $collection);
      _libcat_collection_save_revision($collection, $account->uid);
      $op = 'insert';
    }
    else {
      // For existing collections, update the collection record which matches the
      // $collection->collection_id.
      drupal_write_record('libcat_collection', $collection, 'collection_id');
      // Then, if a new componet revision was requested, save a new record for
      // that; otherwise, update the collection revision record which matches the
      // value of $collection->collection_vid.
      if (!empty($collection->revision)) {
        _libcat_collection_save_revision($collection, $account->uid);
      }
      else {
        _libcat_collection_save_revision($collection, $account->uid, 'collection_vid');
        $update_collection = FALSE;
      }
      $op = 'update';
    }
    if ($update_collection) {
      db_update('libcat_collection')
        ->fields(array('collection_vid' => $collection->collection_vid))
        ->condition('collection_id', $collection->collection_id)
        ->execute();
    }

    // Save fields.
    $function = "field_attach_$op";
    $function('libcat_collection', $collection);

    module_invoke_all('libcat_collection_' . $op, $collection);
    module_invoke_all('entity_' . $op, $collection, 'libcat_collection');

    // Clear internal properties.
    unset($collection->is_new);
    unset($collection->original);
    // Clear the static loading cache.
    entity_get_controller('libcat_collection')->resetCache(array($collection->collection_id));

    // Ignore slave server temporarily to give time for the
    // saved entity to be propagated to the slave.
    db_ignore_slave();
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('libcat_collection', $e);
    throw $e;
  }
}

/**
 * Saves a revision of a business collection.
 *
 * @param object $collection
 *   A business collection object.
 * @param int $uid
 *   The user ID of the user making the revision.
 * @param array $update
 *   (Optional) The key or keys to update on.
 */
function _libcat_collection_save_revision($collection, $uid, $update = array()) {
  $temp_uid = $collection->uid;
  $collection->uid = $uid;
  drupal_write_record('libcat_collection_revision', $collection, $update);
  // Have collection object still show author's uid, not revision author's.
  $collection->uid = $temp_uid;
}

/**
 * Load a collection given a collection name.
 *
 * NOTE TODO: Collection names may not currently be enforced unique...
 *
 * @param string $collection_name
 * @return object $collection
 */
function libcat_collection_load_by_name($name) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', LIBCAT_COLLECTION_ENTITY_TYPE)
        ->propertyCondition('name', $name);
  $result = $query->execute();
  if (!$result) {
    // There is collection that matches this name.
    return NULL;
  }
  // We assume that there is only one match.
  $collection_id_array = array_keys($result[LIBCAT_COLLECTION_ENTITY_TYPE]);
  $collection_id = $collection_id_array[0];
  $collection = libcat_collection_load($collection_id);
  return $collection;
}

/**
 * Load collections by e-mail address of owner.
 *
 * This does *not* include the e-mails of mere managers.
 *
 * Returns an array of one or more collections, or NULL if none match.
 */
function libcat_collection_load_by_owner_email($mail) {
  $account = user_load_by_mail($mail);
  if (!$account) {
    return NULL;
  }
  return libcat_collection_load_by_user($account->uid);
}

/**
 * Load collections by owner user ID.
 *
 * This does *not* include the user IDs of mere managers.
 *
 * Returns an array of one or more collections, or NULL if none match.
 */
function libcat_collection_load_by_user($uid) {
  // Allow account objects instead of simply UID.
  $uid = (is_object($uid)) ? $uid->uid : $uid;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', LIBCAT_COLLECTION_ENTITY_TYPE)
        ->propertyCondition('uid', $uid);
  $result = $query->execute();
  if (!$result) {
    // There is no collection yet that matches this uid.
    return NULL;
  }
  $collection_id_array = array_keys($result[LIBCAT_COLLECTION_ENTITY_TYPE]);
  $collections = libcat_collection_load_multiple($collection_id_array);
  return $collections;
}

/**
 * Loads a single collection based on user ID and collection type.
 *
 * This only makes sense for collection types that only allow one collection per
 * user, such as personal.
 */
function libcat_collection_load_by_type_uid($collection_type, $uid) {
  // Allow account objects instead of simply UID.
  $uid = (is_object($uid)) ? $uid->uid : $uid;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', LIBCAT_COLLECTION_ENTITY_TYPE)
        ->propertyCondition('collection_type', $collection_type)
        ->propertyCondition('uid', $uid);
  $result = $query->execute();
  if (!$result) {
    // There is no collection yet that matches this collection type and uid.
    return NULL;
  }
  // We know that the result of that query will be only one collection.
  // @TODO this is only true of personal collections
  $collection_id_array = array_keys($result[LIBCAT_COLLECTION_ENTITY_TYPE]);
  $collection_id = $collection_id_array[0];
  $collection = libcat_collection_load($collection_id);
  return $collection;
}

/**
 * Loads a single collection.
 */
function libcat_collection_load($collection_id = NULL, $collection_vid = NULL, $reset = FALSE) {
  $collection_ids = isset($collection_id) ? array($collection_id) : array();
  $conditions = isset($collection_vid) ? array('collection_vid' => $collection_vid) : array();
  $collection = libcat_collection_load_multiple($collection_ids, $conditions, $reset);
  return $collection ? reset($collection) : FALSE;
}

/**
 * Loads multiple collections.
 */
function libcat_collection_load_multiple($collection_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('libcat_collection', $collection_ids, $conditions, $reset);
}

/**
 * Helper function to generate standard collection permissions.
 *
 * Optionally accepts a type parameter and will return only permissions for
 * that type.
 *
 * @param $type
 *   (Optional) The machine-readable name of the collection type.
 * @return array
 *   An array of permission names and descriptions.
 */
function libcat_collection_list_permissions($type = NULL) {
  $perms = array();

  // Collection autocomplete needs access to any collection regardless of type.
  $perms += array(
    "view any libcat_collection" => array(
      'title' => t('View any collection'),
    ),
  );
  $types = libcat_collection_types();

  // For the special case of only loading one type's permissions, drop others.
  if ($type) {
    $info = $types[$type];
    $types = array();
    $types[$type] = $info;
  }

  foreach ($types as $type => $info) {
    $perms += array(
      "view own $type libcat_collection" => array(
        'title' => t('%type_name: View own collection', array('%type_name' => $info->name)),
      ),
      "view any $type libcat_collection" => array(
        'title' => t('%type_name: view any collection', array('%type_name' => $info->name)),
      ),
      "create $type libcat_collection" => array(
        'title' => t('%type_name: Create new collection', array('%type_name' => $info->name)),
      ),
      "edit own $type libcat_collection" => array(
        'title' => t('%type_name: Edit own collection', array('%type_name' => $info->name)),
      ),
      "edit any $type libcat_collection" => array(
        'title' => t('%type_name: Edit any collection', array('%type_name' => $info->name)),
      ),
      "delete own $type libcat_collection" => array(
        'title' => t('%type_name: Delete own collection', array('%type_name' => $info->name)),
      ),
      "delete any $type libcat_collection" => array(
        'title' => t('%type_name: Delete any collection', array('%type_name' => $info->name)),
      ),
    );
  }
  return $perms;
}

/**
 * Determine whether a user may perform a given operation on a given collection.
 *
 * @param $op
 *   The operation to be performed on the collection. Possible values are:
 *   - "view"
 *   - "update"
 *   - "delete"
 *   - "create"
 * @param $collection
 *   The collection object on which the operation is to be performed, or
 *   collection type (for example, 'institutional') for "create" operation.
 * @param $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 * @param $target_account
 *   For viewing or editing personal collections without having to load the
 *   personal collection in the menu to check access, this is the account
 *   being visited, as at 'user/%user/collection'.
 * @return
 *   TRUE if the operation may be performed, FALSE otherwise.
 */
function libcat_collection_access($op, $collection, $account = NULL, $target_account = NULL) {
  $rights = &drupal_static(__FUNCTION__, array());

  if (!$collection || !in_array($op, array('view', 'update', 'delete', 'create'), TRUE)) {
    // If there was no collection to check against, or the $op was not one of
    // the supported ones, we return access denied.
    return FALSE;
  }
  // If no user object is supplied, the access check is for the current user.
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }

  // $collection may be either an object or a collection type (string).  Use
  // either collection id or type as the static cache id.
  $cid = is_object($collection) ? $collection->collection_id : $collection;

  // If we've already checked access for this collection, user and op, return
  // from cache.
  if (isset($rights[$account->uid][$cid][$op])) {
    return $rights[$account->uid][$cid][$op];
  }

  // Invoke hook_libcat_collection_access to see if other modules allow or deny.
  $access = module_invoke_all('libcat_collection_access', $op, $collection, $account);
  // If any module explicitly returns false, deny access.
  if (in_array(FALSE, $access, TRUE)) {
    $rights[$account->uid][$cid][$op] = FALSE;
    return FALSE;
  }
  // If a module explicitly grants access, grant access.
  if (in_array(TRUE, $access, TRUE)) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }

  // If not overridden above, determination of access based on permissions.
  $type = is_string($collection) ? $collection : $collection->collection_type;
  if ($target_account) {
    // Create faux collection object for 'view own' and 'edit own' permissions.
    $collection = new stdClass();
    $collection->uid = $target_account->uid;
  }
  if ($op == 'view') {
    if (user_access('view any libcat_collection', $account) || user_access('view any ' . $type . ' libcat_collection', $account) || (user_access('view own ' . $type . ' libcat_collection') && ($account->uid == $collection->uid))) {
      $rights[$account->uid][$cid][$op] = TRUE;
      return TRUE;
    }
  }
  elseif ($op == 'create' && user_access('create ' . $type . ' libcat_collection', $account)) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }
  elseif ($op == 'update') {
    if (user_access('edit any ' . $type . ' libcat_collection', $account) || (user_access('edit own ' . $type . ' libcat_collection', $account) && ($account->uid == $collection->uid))) {
      $rights[$account->uid][$cid][$op] = TRUE;
      return TRUE;
    }
  }
  elseif ($op == 'delete') {
    if (user_access('delete any ' . $type . ' libcat_collection', $account) || (user_access('delete own ' . $type . ' libcat_collection', $account) && ($account->uid == $collection->uid))) {
      $rights[$account->uid][$cid][$op] = TRUE;
      return TRUE;
    }
  }

  // Collection owners can always view their collection.
  if ($op == 'view' && $account->uid == $collection->uid && $account->uid != 0) {
    $rights[$account->uid][$cid][$op] = TRUE;
    return TRUE;
  }

  // By default, deny access.
  $rights[$account->uid][$cid][$op] = FALSE;
  return FALSE;
}

/**
 * Generate an array for rendering the given collection.
 *
 * @param $collection
 *   A collection object.
 * @param $view_mode
 *   View mode, e.g. 'full', 'teaser'...
 * @param $langcode
 *   (optional) A language code to use for rendering. Defaults to the global
 *   content language of the current request.
 *
 * @return
 *   An array as expected by drupal_render().
 */
function libcat_collection_view($collection, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Populate $collection->content with a render() array.
  libcat_collection_build_content($collection, $view_mode, $langcode);

  $build = $collection->content;
  // We don't need duplicate rendering info in collection->content.
  unset($collection->content);

  $build += array(
    '#theme' => 'libcat_collection',
    '#libcat_collection' => $collection,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  );

  // Add contextual links for this entity. Modules may alter this behavior (for
  // example to restrict contextual links to certain view modes) by implementing
  // hook_libcat_collection_view_alter().
  if (!empty($collection->collection_id)) {
    $build['#contextual_links']['libcat_collection'] = array('libcat_collection', array($collection->collection_id));
  }

  // Allow modules to modify the structured collection.
  $type = 'libcat_collection';
  drupal_alter(array('libcat_collection_view', 'entity_view'), $build, $type);
  return $build;
}

/**
 * Builds a structured array representing the collection's content.
 *
 * The content built for the collection (field values or other collection
 * components) will vary depending on the $view_mode parameter.
 *
 * We defines the following view modes for collections, with these use cases:
 *   - full (default): full collection is being displayed.
 *   - teaser: an intro to the collection is displayed, as for listing pages.
 * Other modules might define additional view modes, or use existing view modes
 * in additional contexts.
 *
 * @param $collection
 *   A collection object.
 * @param $view_mode
 *   View mode, e.g. 'full', 'teaser'...
 * @param $langcode
 *   (optional) A language code to use for rendering. Defaults to the global
 *   content language of the current request.
 */
function libcat_collection_build_content($collection, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Remove previously built content, if exists.
  $collection->content = array();

  // Build fields content.
  // In case of a multiple view, libcat_collection_view_multiple() already ran
  // the 'prepare_view' step. An internal flag prevents the operation from
  // running twice.
  field_attach_prepare_view('libcat_collection', array($collection->collection_id => $collection), $view_mode, $langcode);
  entity_prepare_view('libcat_collection', array($collection->collection_id => $collection), $langcode);
  $collection->content += field_attach_view('libcat_collection', $collection, $view_mode, $langcode);

  // Always display a read more link on teasers because we have no way
  // to know when a teaser view is different than a full view.
  $links = array();
  $collection->content['links'] = array(
    '#theme' => 'links__libcat_collection',
    '#pre_render' => array('drupal_pre_render_links'),
    '#attributes' => array('class' => array('links', 'inline')),
  );
  $collection->content['links']['libcat_collection'] = array(
    '#theme' => 'links__libcat_collection__libcat_collection',
    '#links' => $links,
    '#attributes' => array('class' => array('links', 'inline')),
  );

  // Allow modules to make their own additions to the collection.
  module_invoke_all('libcat_collection_view', $collection, $view_mode, $langcode);
  module_invoke_all('entity_view', $collection, 'libcat_collection', $view_mode, $langcode);
}

/**
 * Prepares a collection object for editing.
 *
 * Fills in a few default values and invokes hook_libcat_collection_prepare().
 */
function libcat_collection_object_prepare($collection) {
  // Set up default values, if required.
  $collection_options = variable_get('libcat_collection_options_' . $collection->collection_type, array('revision'));
  // If this is a new collection, fill in the default values.
  if (!isset($collection->collection_id) || isset($collection->is_new)) {

    global $user;
    $collection->uid = $user->uid;
    $collection->updated = REQUEST_TIME;
  }
  else {
    $collection->updated = REQUEST_TIME;
  }
  // Always use the default revision setting.
  $collection->revision = in_array('revision', $collection_options);

//  libcat_collection_invoke($collection, 'prepare');
  module_invoke_all('libcat_collection_prepare', $collection);
}

/**
 * Helper function to determine if a user has access to create any collection.
 *
 * Used for the libcat/collection/add menu item.
 */
function _libcat_collection_add_access() {
  $types = libcat_collection_types();
  foreach ($types as $type => $info) {
    if (libcat_collection_access('create', $type)) {
      // libcat_collection_hook($type, 'form') &&   ??
      return TRUE;
    }
  }
  return FALSE;
}